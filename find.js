// Navbar

window.onscroll = function(event) {
    let navbar = document.querySelector(".presto-navbar")
    if (document.documentElement.scrollTop > 200) {
        navbar.classList.add ('padding-extension')
    } else {
        navbar.classList.remove('padding-extension')
        
    }
}

let buttonToggler = document.querySelector('.navbar .navbar-toggler')
let togglerIcon = document.querySelector('.navbar .navbar-toggler i')
togglerIcon.classList.add("rotate-0")
buttonToggler.addEventListener("click", function() {
    togglerIcon.classList.toggle("rotate-90")
})


// Annunci

fetch('./annunci.json')
.then( (response) => response.json() )                    
.then( (announcements) => {
    
    //  Estrazione categorie e struttura checkbox
    
    let categories = new Set();
    announcements.forEach((el) => categories.add(el.category));
    
    categories.forEach(function(category) {
        let categoryElement = document.createElement("div");
        categoryElement.classList.add('presto-category-find');
        
        
        categoryElement.innerHTML = 
        `
        <div class="form-check fw-bolder">
        <input class="form-check-input" type="checkbox" value="${category}" id="checkbox${category}">
        <label class="form-check-label" for="checkbox${category}">
        ${category}
        </label>
        </div>
        
        `
        categoryElement.addEventListener('input', allFilters);
        
        let categoriesWrapper = document.querySelector("#accordionCategoryWrapper");
        categoriesWrapper.appendChild(categoryElement)
    })
    
    
    // trovo gli estremi
    let maxPrice = Number(announcements[0].price);
    let minPrice = Number(announcements[0].price);
    
    announcements.forEach(function(announcement, index) {
        if(index == 0) return;
        
        let price = Number(announcement.price);
        
        if(price > maxPrice) {
            maxPrice = price;
        };                        
        
        if(price < minPrice) {
            minPrice = price;
        }
        
    });
    
    // noUiSlider
    let slider = document.querySelector("#range-price");
    
    noUiSlider.create(slider, {
        start: [minPrice, maxPrice],
        connect: true,
        // tooltips: [true, true],
        range: {
            'min': 0,
            'max': maxPrice
        }
    });
    
    let connect = slider.querySelector('.noUi-connect');
    connect.classList.add("presto-bg");
    
    // Binding e filtraggio
    slider.noUiSlider.on('change', allFilters);
    
    let radioSort = document.querySelectorAll(".presto-radio-sort");
    radioSort.forEach(function(node) {
        node.addEventListener('input', allFilters);
    })
    
    slider.noUiSlider.on('update', function() {
        let rangeMin = document.querySelector("#rangeMin");
        let rangeMax = document.querySelector("#rangeMax");
        let sliderParams = slider.noUiSlider.get();
        rangeMin.innerHTML = `${sliderParams[0]}`
        rangeMax.innerHTML = `${sliderParams[1]}`
    })
    
    // ends noUiSlider
    
    annunciPopulate(announcements);
    
    let search = document.querySelector("#searchName");
    search.addEventListener("input", allFilters)
    
    
    
    
    
    // funzione per un filtro generale combinato per i tre criteri di ricerca
    function allFilters() {
        let categoryInputs = document.querySelectorAll(".presto-category-find .form-check .form-check-input");
        let checkedCategories = [];
        categoryInputs.forEach(function(inputElements) {
            if(inputElements.checked) {
                checkedCategories.push(inputElements.value);
            }
            
        })
        let searchName = document.querySelector("#searchName");
        let lowerSearch = searchName.value.toLowerCase();
        let sliderParams = slider.noUiSlider.get();
        let min = sliderParams[0];
        let max = sliderParams[1];
        
        // Filtro composto
        
        let filteredAnnouncements = 
        announcements.filter((announcement) => !lowerSearch || announcement.name.toLowerCase().includes(lowerSearch))             // il risultato di questo filtro lo rifiltro dopo
        .filter((announcement) => checkedCategories.length == 0 || checkedCategories.includes(announcement.category))
        .filter((announcement) => Number(announcement.price) >= min &&  Number(announcement.price) <= max )
        
        annunciPopulate(filteredAnnouncements); 
        
        let radioSort = document.querySelectorAll(".presto-radio-sort");
        Array.from(radioSort).forEach(function(radio, index) {
            if (radio.checked) {
                switch (index) {               
                    // Data crescente
                    case 0:                                 // se l'indice è zero filtra in questo modo
                    filteredAnnouncements.sort((a, b) => a.id - b.id);
                    break;
                    
                    // Data decrescente
                    case 1:
                    filteredAnnouncements.sort((a, b) => b.id - a.id);
                    break;
                    
                    // Prezzo crescente
                    case 2:
                    filteredAnnouncements.sort((a, b) => Number(a.price) - Number(b.price));
                    break;
                    
                    // Prezzo decrescente
                    case 3:
                    filteredAnnouncements.sort((a, b) => Number(b.price) - Number(a.price));
                    break;
                    
                    // Nome crescente
                    case 4:
                    filteredAnnouncements.sort((a, b) => {
                        var nameA = a.name.toLowerCase();
                        var nameB = b.name.toLowerCase();
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        
                        // names must be equal
                        return 0;
                    });
                    break;
                    
                    // Nome decrescente
                    case 5:
                    filteredAnnouncements.sort((a, b) => {
                        var nameA = a.name.toLowerCase();
                        var nameB = b.name.toLowerCase();
                        if (nameA > nameB) {
                            return -1;
                        }
                        if (nameA < nameB) {
                            return 1;
                        }
                        
                        // names must be equal
                        return 0;
                    });
                    break;
                    
                    default:
                    break;
                }
            }
        })
        
        annunciPopulate(filteredAnnouncements);
    }
    
    // function nameFilter() {
    //     let searchName = document.querySelectorAll("#searchName");
    //     let lowerSearch = searchName.value.toLowerCase();
    
    //     // Filtro annunci per nome
    
    //     if( lowerSearch ) {  
    //         let filteredAnnouncements = announcements.filter((announcement) => announcement.name.toLowerCase().includes(lowerSearch)); 
    //         annunciPopulate(filteredAnnouncements);         
    //     } else {
    
    //         annunciPopulate(announcements);                  // se la stringa di ricerca è vuota, non c'è, è falsy, popoliamo la pagina con gli annunci
    
    //     }
    // }
    
    // function categoryFilter() {
    //     let categoryInputs = document.querySelectorAll(".presto-category-find .form-check .form-check-input");
    //     let checkedCategories = [];
    //     categoryInputs.forEach(function(inputElements) {
    //         if(inputElements.checked) {
    //             checkedCategories.push(inputElements.value);
    //         }
    
    //     })
    
    //     // Filtro gli annunci per categoria
    //     if(checkedCategories.length > 0 ) {
    //         let filteredAnnouncements = announcements.filter((announcement) => checkedCategories.includes(announcement.category)); // includes restituisce true se la categoria si trova in checkedCategories oppure false
    //         annunciPopulate(filteredAnnouncements);
    //     } else {
    //         annunciPopulate(announcements);
    //     }
    // }
    
    
    // Tutti gli annunci
    
    function annunciPopulate(announcements) {
        
        let annunciWrapper = document.querySelector("#annunciWrapper");
        annunciWrapper.innerHTML = "";
        
        announcements.forEach(function(announcement) {
            
            let announcementElement = document.createElement("div")
            announcementElement.classList.add("presto-find-card")
            announcementElement.classList.add("mb-4")
            announcementElement.classList.add("mx-4")
            announcementElement.classList.add("text-center")
            
            announcementElement.innerHTML = 
            `
            <h4 class="fw-bolder">${announcement.category}</h4>
            <img class ="rounded-3" src="https://picsum.photos/200">
            <div class="d-flex flex-column my-3">
            <span>${announcement.name}</span>
            <span>€ ${announcement.price}</span>
            </div>
            
            `
            annunciWrapper.appendChild(announcementElement);
            
        });
        
    }
    
    // // Filtro annunci per nome
    
    // let searchName = document.querySelector("#searchName");
    // searchName.addEventListener("input", function () {
    
    //     let lowerSearch = searchName.value.toLowerCase();
    
    //     // Filtro annunci per nome
    
    //     if( lowerSearch ) {  
    //         let filteredAnnouncements = announcements.filter((announcement) => announcement.name.toLowerCase().includes(lowerSearch)); 
    //         annunciPopulate(filteredAnnouncements);         
    //     } else {
    
    //         annunciPopulate(announcements);                  // se la stringa di ricerca è vuota, non c'è, è falsy, popoliamo al apgina con gli annunci
    
    //     }
    
    // })
    
});







