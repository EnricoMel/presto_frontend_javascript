// Navbar

window.onscroll = function(event) {
    let navbar = document.querySelector(".presto-navbar")
    if (document.documentElement.scrollTop > 200) {
        navbar.classList.add ('padding-extension')
    } else {
        navbar.classList.remove('padding-extension')
        
    }
    
}

let buttonToggler = document.querySelector('.navbar .navbar-toggler')
let togglerIcon = document.querySelector('.navbar .navbar-toggler i')
togglerIcon.classList.add("rotate-0")
buttonToggler.addEventListener("click", function() {
    togglerIcon.classList.toggle("rotate-90")
})

// /* Categorie */

// let categories = [
//     {
//         titolo: "Abbigliamento",
//         icona: "tshirt"

//     },
//     {
//         titolo: "Razzi",
//         icona: "rocket"

//     },
//     {
//         titolo: "Case",
//         icona: "home"

//     },
//     {
//         titolo: "Automobili",
//         icona: "car"

//     },
//     {
//         titolo: "Motociclette",
//         icona: "motorcycle"

//     },
//     {
//         titolo: "Giochi",
//         icona: "gamepad"

//     },
//     {
//         titolo: "Elettronica",
//         icona: "laptop"

//     },
//     {
//         titolo: "Giardinaggio",
//         icona: "seedling"

//     },
// ]


// FETCH E PROMISE E RECUPERIAMO I DATI CHE CI SERVONO DA UN API

// let p = fetch("https://pokeapi.co/api/v2/pokemon/ditto"); // API, una url che restituisce un json
// let p1 = p.then( function(response) {
//     return response.json();
// });

// p1.then(function(json) {
//     console.log(`Peso ${json.weight}`);      // mi ritorna un oggetto, che non ha nessun tipo e deriva da Object, e sono tutti i dati che mi ha passato la chiamata all'API 
// })

// fetch("https://pokeapi.co/api/v2/pokemon/ditto").then(function(response) {
//     return response.json();
// }).then(function(json) {
//     console.log(json);
// })

// USO DELLA FETCH SUL MIO WEB SERVER

// fetch("annunci.json").then(function(response) {
//     return response.json();
// }).then(function(a) {
//     console.log(a);
// })

var perView = 2;


fetch('./annunci.json')
.then( (response) => response.json() )                    // converte il file json in un array, in un oggetto JS, comprensibile al JS
.then( (announcements) => {
    
    // Estrazione categorie
    
    let categories = new Set();
    announcements.forEach((el) => categories.add(el.category));
    
    categories.forEach(function(category) {
        let categoryElement = document.createElement("div")
        categoryElement.className = "presto-card presto-text text-center d-flex flex-column shadow m-1"
        
        categoryElement.innerHTML = 
        `
        <p class="h2 fw-bolder mt-3">
        ${category}
        </p>
        
        `
        categoryElement.addEventListener("mouseenter", function() {
            categoryElement.classList.remove("shadow")
            categoryElement.classList.add("shadow-lg")
            
        })
        categoryElement.addEventListener("mouseleave", function() {
            categoryElement.classList.remove("shadow-lg")
            categoryElement.classList.add("shadow")
            
        })
        
        let categoriesWrapper = document.querySelector("#categories-wrapper")
        categoriesWrapper.appendChild(categoryElement)
    })
        
    // Ultimi annunci in evidenza col metodo slice()
    
    // li ordino prima per id in ordine decrescente
    
    announcements.sort((a, b) => b.id - a.id );
    
    announcements.slice(0, 6).forEach(function(announcement, indice) {
        let announcementElement = document.createElement("div")
        announcementElement.classList.add("glide__slide")
        announcementElement.classList.add("mb-4")
        
        announcementElement.innerHTML = 
        `
        <h4 class="fw-bolder">${announcement.category}</h4>
        <img class ="rounded-3" src="https://picsum.photos/200">
        <div class="d-flex flex-column my-3">
        <span>${announcement.name}</span>
        <span>${announcement.price}</span>
        </div>
        `
        
        let glideSlidesWrapper = document.querySelector(".glide .glide__track .glide__slides")
        glideSlidesWrapper.appendChild(announcementElement);
        
        
        if(indice % perView == 0) {
            let announcementBullett = document.createElement("button")
            announcementBullett.classList.add("glide__bullet")
            announcementBullett.classList.add("presto-bullet-button")
            announcementBullett.setAttribute("data-glide-dir", `=${indice}`)
            
            
            let glideBulletsWrapper = document.querySelector(".glide .glide__bullets")
            glideBulletsWrapper.appendChild(announcementBullett);
            
        }
    });
    
    new Glide('.glide', {
        type: 'carousel',
        startAt: 0,
        perView: perView              // n. di slide nella view
    }).mount()
});


// let announcements = [
//     {
//         utente: "user1",
//         fotoUrl: "https://picsum.photos/200",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
//     {
//         utente: "user2",
//         fotoUrl: "https://picsum.photos/201",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
//     {
//         utente: "user3",
//         fotoUrl: "https://picsum.photos/202",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
//     {
//         utente: "user4",
//         fotoUrl: "https://picsum.photos/203",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
//     {
//         utente: "user5",
//         fotoUrl: "https://picsum.photos/204",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
//     {
//         utente: "user6",
//         fotoUrl: "https://picsum.photos/205",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
//     {
//         utente: "user7",
//         fotoUrl: "https://picsum.photos/206",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
//     {
//         utente: "user8",
//         fotoUrl: "https://picsum.photos/207",
//         annuncio: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat excepturi, libero temporibus tenetur sit explicabo saepe est minima ratione delectus error distinctio cumque iste illum, soluta quo officiis tempore similique."

//     },
// ]




